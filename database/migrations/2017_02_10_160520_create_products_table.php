<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('category_id');
            $table->string('name');
            $table->string('slug');
            $table->text('description');
            $table->double('price');
            $table->integer('stock')->default(0);
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
        });

        DB::table('products')->insert([
           ['category_id' => '3', 'name' => 'Bombay Sapphire', 'slug' => 'bombay-sapphire', 'description' => 'This is a very dry gin.', 'price' => '21.99', 'stock' => '100'],
           ['category_id' => '5', 'name' => 'Jameson', 'slug' => 'jameson', 'description' => 'A very strong flavoured whiskey.', 'price' => '29.99', 'stock' => '100'],
           ['category_id' => '5', 'name' => 'The Famous Grouse', 'slug' => 'the-famous-grouse', 'description' => 'A bi-loved whiskey in Scotland.', 'price' => '21.99', 'stock' => '100'],
           ['category_id' => '3', 'name' => 'Hendrick\'s', 'slug' => 'hendrick-s', 'description' => 'A must have.', 'price' => '31.79', 'stock' => '0'],
           ['category_id' => '7', 'name' => 'Grey Goose Vodka', 'slug' => 'grey-goose-vodka', 'description' => 'The premium vodka Gray Goose is made in the famous Cognac region of Western France. The ideal combination of experience, knowledge of Cognac by the environment and the current vision of making vodka, the quality vodka Gray Goose originated.', 'price' => '38.99', 'stock' => '100'],
           ['category_id' => '7', 'name' => 'Absolut Vodka', 'slug' => 'absolut-vodka', 'description' => 'Vodka of excellent quality. Made from only natural ingredients. The best winter wheat and water from its own source. No added sugars.', 'price' => '18.49', 'stock' => '100'],
           ['category_id' => '8', 'name' => 'Petrikov Red', 'slug' => 'petrikov-red', 'description' => 'Petrikov is the perfect blend of the purest Petrikov vodka and juices and aromas of fresh fruit. For Petrikov Red is the pure natural juice of blood oranges, the basis for this red vodka. Petrikov is wonderfully pure to drink, but also to mix. Try it with energy drink, orange juice or 7-UP / Sprite. With Petrikov can be endlessly varied and make a delicious fresh mix in a snap!', 'price' => '8.69', 'stock' => '100'],
           ['category_id' => '5', 'name' => 'Glenfiddich 12 Years', 'slug' => 'glenfiddich-12-years', 'description' => 'The Glenfiddich distillery is located in the Scottish place Dufftown in the Speyside region and was founded in 1886 by William Grant. This whiskey is aged at least 12 years in barrels of American oak where formerly Bourbon is aged in barrels and Spanish oak sherry which previously has matured. the vessels by the Malt Master selected after 12 years and poured in a wooden \'marrying tun "where the whiskey can marry each other to form a nicely balanced whole in a few months.', 'price' => '32.79', 'stock' => '100'],
           ['category_id' => '6', 'name' => 'Jack Daniels Honey ', 'slug' => 'jack-daniels-honey-tennessee ', 'description' => 'Jack Daniel\'s Tennessee Honey (35% alcohol) is the original Jack Daniel\'s Old No. 7 whiskey mixed with three types of 100% natural American honey. You taste the distinct flavor of the whiskey familiar, but mixed with a hint of natural honeycomb, sweet molasses, roasted nuts and hint of chocolate and praline.', 'price' => '28.99', 'stock' => '100'],
           ['category_id' => '3', 'name' => 'Gordon\'s London Dry ', 'slug' => 'gordons-london-dry ', 'description' => 'The refreshing juniper is the heart of Gordon\'s Gin. The base gets its taste from the blend of natural herbs and spices, called \'botanicals\'. A slight citrus flavor completes the whole. The gin has been distilled twice for a higher purity and the inclusion of the plant based ingredients. All of gin mixed with tonic in 1858 and is still the most tasteful combination.', 'price' => '23.49', 'stock' => '100'],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
