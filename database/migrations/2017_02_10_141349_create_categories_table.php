<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('category_id')->default(0);
            $table->string('name');
            $table->string('slug');
        });

        DB::table('categories')->insert([
            ['name' => 'None', 'slug' => 'none', 'category_id' => '0'],
            ['name' => 'Gin', 'slug' => 'gin', 'category_id' => '0'],
            ['name' => 'Dry Gin', 'slug' => 'dry-gin', 'category_id' => '2'],
            ['name' => 'Whiskey', 'slug' => 'whiskey', 'category_id' => '0'],
            ['name' => 'Scottish Whiskey', 'slug' => 'scottish-whiskey', 'category_id' => '4'],
            ['name' => 'American Whiskey', 'slug' => 'american-whiskey', 'category_id' => '4'],
            ['name' => 'Vodka', 'slug' => 'vodka', 'category_id' => '0'],
            ['name' => 'Flavored Vodka', 'slug' => 'flavored-vodka', 'category_id' => '7'],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }
}
