<?php

namespace App\Http\Requests\Product\Review;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'body' => 'required|max:2000',
        ];
    }

    public function persist()
    {
        $this->route('product')->reviews()->create([
            'user_id' => currentUser()->id,
            'body' => $this->body,
        ]);
    }
}
