<?php

namespace App\Http\Requests\Category;

use App\Models\Category;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'category_id' => Rule::exists('categories', 'id'),
        ];
    }

    /**
     * Create a new category.
     *
     * @return Category
     */
    public function persist()
    {
        return Category::create($this->only('name', 'category_id'));
    }
}
