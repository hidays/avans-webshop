<?php

namespace App\Http\Requests\Search;

use App\Models\Product;
use App\Models\Category;
use Illuminate\Foundation\Http\FormRequest;

class SearchRequest extends FormRequest
{
    /**
     * @var string
     */
    protected $redirect = '/search';

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    /**
     * Search for products.
     *
     * @return array
     */
    public function persist()
    {
        $results = ['products' => [], 'categories' => []];

        $searchQuery = $this->input('query');

        if (! is_null($searchQuery)) {
            \View::share('searchQuery', $searchQuery);

            $products = $this->getProducts($searchQuery);
            $categories = $this->getCategories($searchQuery);

            $results = compact('products', 'categories');
        }

        return $results;
    }

    /**
     * Search products by search query.
     *
     * @param  string  $query
     * @return mixed
     */
    private function getProducts($query)
    {
        return Product::where('name', 'like', "%{$query}%")
            ->orWhere('description', 'like', "%{$query}%")
            ->get();
    }

    /**
     * Search categories by search query.
     *
     * @param  string  $query
     * @return mixed
     */
    private function getCategories($query)
    {
        return Category::where('name', 'like', "%{$query}%")
            ->get();
    }
}
