<?php

namespace App\Http\Controllers;

use App\Models\Activation;

class ActivationController extends Controller
{
    /**
     * Activate the account for the given user.
     *
     * @param  string  $token
     * @return \Illuminate\Http\RedirectResponse
     */
    public function show($token)
    {
        $activation = Activation::where('token', $token)->first();

        if (is_null($activation)) {
            abort(404);
        }

        $activation->delete();

        return redirect()->route('home');
    }
}
