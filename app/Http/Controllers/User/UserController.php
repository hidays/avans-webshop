<?php

namespace App\Http\Controllers\User;

use App\Models\User;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    /**
     * Show the user profile.
     *
     * @param User|null $user
     * @return \Illuminate\View\View
     */
    public function show(User $user = null)
    {
        if (is_null($user) || ! $user->exists) {
            $user = currentUser();

            return view('user.show', compact('user'));
        }

        return view('user.show', compact('user'));
    }
}