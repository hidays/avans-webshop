<?php

namespace App\Http\Controllers;

use App\Http\Requests\Checkout\StoreRequest;

class CheckoutController extends Controller
{
    /**
     * Make a payment.
     *
     * @param StoreRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StoreRequest $request)
    {
        $request->persist();

        return redirect()->route('thank-you');
    }
}
