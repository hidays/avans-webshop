<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\Product;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::orderBy('created_at', 'desc')->limit(12)->get();

        return view('home', compact('products'));
    }

    public function thanks()
    {
        $order = Order::latest()->first();

        return view('thanks', compact('order'));
    }
}
