<?php

namespace App\Http\Controllers;

use App\Http\Requests\Cart\UpdateRequest;
use App\Models\ShoppingList;
use Illuminate\Http\Request;
use App\Http\Requests\Cart\DestroyRequest;

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('cart.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\Cart\UpdateRequest  $request
     * @param  \App\Models\ShoppingList  $item
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, ShoppingList $item)
    {
        $request->persist();

        return response()->json();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Http\Requests\Cart\DestroyRequest  $request
     * @param  \App\Models\ShoppingList  $item
     * @return \Illuminate\Http\Response
     */
    public function destroy(DestroyRequest $request, ShoppingList $item)
    {
        $request->persist();

        return redirect()->route('cart.index');
    }
}
