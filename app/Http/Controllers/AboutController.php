<?php

namespace App\Http\Controllers;

use App\Models\Configuration;
use App\Http\Requests\Admin\About\UpdateRequest;
use App\Models\Product;

class AboutController extends Controller
{
    /**
     * AboutController constructor.
     */
    public function __construct()
    {
        $this->middleware('admin')->except('index');
    }

    /**
     * Show the about page.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $about = Configuration::findOrFail('about')->value;

        return view('about.index', compact('about'));
    }

    /**
     * Show the edit about page.
     *
     * @return \Illuminate\View\View
     */
    public function edit()
    {
        $about = Configuration::findOrFail('about')->value;

        return view('about.edit', compact('about'));
    }

    /**
     * Update the about page.
     *
     * @param  \App\Http\Requests\Admin\About\UpdateRequest  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateRequest $request)
    {
        $request->persist();

        return redirect()->route('about.index');
    }
}
