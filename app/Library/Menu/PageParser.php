<?php

namespace App\Library\Menu;

use App\Models\Page;

class PageParser
{
    /**
     * Parse a page to HTML.
     *
     * @param  \App\Models\Page  $page
     * @return HtmlString
     */
    public function toHtml(Page $page)
    {
        if (! $this->hasAccess($page)) {
            return new HtmlString();
        }

        return $this->createUrl($page->title, $page->route_name);
    }

    /**
     * Create a URL by it's title and route.
     *
     * @param  string  $title
     * @param  string  $route
     * @return HtmlString
     */
    protected function createUrl($title, $route)
    {
        $url = route($route);

        return with(new HtmlString())
            ->append("<li>")
            ->append("<a href='{$url}'>{$title}</a>")
            ->append("</li>");
    }

    /**
     * Determine is a user has access to a page.
     *
     * @param  \App\Models\Page  $page
     * @return bool
     */
    protected function hasAccess($page)
    {
        switch ($page->needs_authentication) {
            case 0: // User can always view this page.
                return true;
            case 1: // User can only view this page when authenticated.
                return ! is_null(currentUser());
            case 2: // User can only view this page when NOT authenticated.
                return is_null(currentUser());
            case 3: // User can only view this page when he/she is an admin.
                return currentUser() && currentUser()->isAdmin();
        }

        return false;
    }
}