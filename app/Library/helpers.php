<?php

if (! function_exists('cart')) {
    /**
     * Return the cart instance.
     *
     * @return \App\Library\Cart\Cart
     */
    function cart()
    {
        return app('app.cart');
    }
}

if (! function_exists('create_order')) {
    /**
     * Generate a new order.
     *
     * @param  string  $email
     * @param  string  $status
     * @return \App\Models\Order
     */
    function create_order($email, $status = 'placed')
    {
        $data = compact('email', 'status');

        $user = currentUser() ?: \App\Models\User::whereEmail($email)->first();

        if ($user) {
            $data['user_id'] = $user->id;
        }

        $order = \App\Models\Order::create($data);

        return $order;
    }
}

if (! function_exists('currentUser')) {
    /**
     * Get the current authenticated user.
     *
     * @param string $guard
     * @return \App\Models\User|mixed
     */
    function currentUser($guard = 'web')
    {
        return \Auth::guard($guard)->user();
    }
}

if (! function_exists('menu')) {
    /**
     * Load the given menu.
     *
     * @param  integer $menu
     * @param  string  $classes
     * @return \App\Library\Menu\HtmlString
     */
    function menu($menu, $classes = null)
    {
        $loader = app()->make(\App\Library\Menu\MenuLoader::class);

        $loader->setClasses($classes);

        return $loader->parse($menu);
    }
}

if (! function_exists('money')) {
    /**
     * Convert to money format and string.
     *
     * @param  mixed  $value
     * @return string
     */
    function money($value)
    {
        return '€ ' . money_format('%.2n', $value);
    }
}

if (! function_exists('image')) {
    /**
     * Upload all images.
     *
     * @param  mixed  $model
     * @return \App\Library\ImageUpload
     */
    function image($model)
    {
        return app('app.image')->setModel($model);
    }
}

if (! function_exists('is_admin')) {
    /**
     * Determine if the user is an admin.
     *
     * @param  \App\Models\User $user
     * @return bool
     */
    function is_admin($user = null)
    {
        if (is_null($user) || ! $user instanceof \App\Models\User) {
            $user = currentUser();
        }

        return ! is_null($user) && $user->isAdmin();
    }
}

if (! function_exists('selected')) {
    /**
     * @param \Illuminate\Database\Eloquent\Model $item
     * @param null $default
     * @return string
     */
    function selected($item, $default = null)
    {
        if ($item->id == old('category_id', $default)) {
            return 'selected';
        }

        return '';
    }
}
