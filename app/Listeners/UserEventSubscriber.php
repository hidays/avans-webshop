<?php

namespace App\Listeners;

use App\Notifications\SendActivation;

class UserEventSubscriber
{
    /**
     * Handle user login events.
     *
     * @param  \Illuminate\Auth\Events\Login  $event
     */
    public function onUserLogin($event)
    {
        if (session()->has('cart')) {
            cart()->mergeCarts($event->user);
        }
    }

    /**
     * Handle user logout events.
     *
     * @param  \Illuminate\Auth\Events\Logout  $event
     */
    public function onUserLogout($event)
    {
        \Cookie::queue('cart', $event->user->id);
    }

    /**
     * Handle user register event.
     *
     * @param \Illuminate\Auth\Events\Registered  $event
     */
    public function onUserRegister($event)
    {
        $activation = $event->user->activation()->create([
            'token' => str_random(60),
        ]);

        $event->user->notify(new SendActivation($activation));
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param  \Illuminate\Events\Dispatcher  $events
     */
    public function subscribe($events)
    {
        $events->listen(
            'Illuminate\Auth\Events\Login',
            'App\Listeners\UserEventSubscriber@onUserLogin'
        );

        $events->listen(
            'Illuminate\Auth\Events\Logout',
            'App\Listeners\UserEventSubscriber@onUserLogout'
        );

        $events->listen(
            'Illuminate\Auth\Events\Registered',
            'App\Listeners\UserEventSubscriber@onUserRegister'
        );
    }

}