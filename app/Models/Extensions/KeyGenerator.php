<?php

namespace App\Models\Extensions;

use Webpatser\Uuid\Uuid;

trait KeyGenerator
{
    /**
     * Boot function from laravel.
     */
    protected static function bootKeyGenerator()
    {
        static::creating(function ($model) {
            $model->{$model->getKeyName()} = Uuid::generate()->string;
        });
    }
}