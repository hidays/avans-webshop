<?php

namespace App\Policies;

use App\Models\User;
use App\Models\ShoppingList;
use Illuminate\Auth\Access\HandlesAuthorization;

class ShoppingListPolicy
{
    use HandlesAuthorization;

    /**
     * Before check.
     *
     * @param  \App\Models\User  $user
     * @param  string  $ability
     * @param  \App\Models\ShoppingList  $item
     * @return bool
     */
    public function before($user, $ability, $item = null)
    {
        if ($user->isAdmin()) {
            return true;
        }
    }

    /**
     * Determine whether the user can view the shoppingList.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\ShoppingList  $shoppingList
     * @return mixed
     */
    public function view(User $user, ShoppingList $shoppingList)
    {
        return $this->hasAccess($user, $shoppingList->user_id);
    }

    /**
     * Determine whether the user can create shoppingLists.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return true;
    }

    /**
     * Determine whether the user can update the shoppingList.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\ShoppingList  $shoppingList
     * @return mixed
     */
    public function update(User $user, ShoppingList $shoppingList)
    {
        return $this->hasAccess($user, $shoppingList->user_id);
    }

    /**
     * Determine whether the user can delete the shoppingList.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\ShoppingList  $shoppingList
     * @return mixed
     */
    public function delete(User $user = null, ShoppingList $shoppingList)
    {
        return $this->hasAccess($user, $shoppingList->user_id);
    }

    /**
     * Determine if the user has access to shopping list item.
     *
     * @param  $user  User|string
     * @param  $idFromItem string
     * @return bool
     */
    private function hasAccess($user, $idFromItem)
    {
        if (is_null($user) || ! $user->exists) {
            $user = cart()->getUser();
        } else {
            $user = $user->id;
        }

        return $user == $idFromItem;
    }
}
