<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Order;
use Illuminate\Auth\Access\HandlesAuthorization;

class OrderPolicy
{
    use HandlesAuthorization;

    /**
     * Determine if user may view order.
     *
     * @param  User  $user
     * @param  Order  $order
     * @return bool
     */
    public function show(User $user, Order $order)
    {
        return $user->id == $order->user_id || $user->isAdmin();
    }
}
