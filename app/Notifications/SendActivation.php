<?php

namespace App\Notifications;

use App\Models\Activation;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class SendActivation extends Notification
{
    use Queueable;

    /**
     * The activation instance.
     *
     * @var \App\Models\Activation
     */
    private $activation;

    /**
     * Create a new notification instance.
     *
     * @param  \App\Models\Activation  $activation
     */
    public function __construct(Activation $activation)
    {
        $this->activation = $activation;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('Welcome ' . $notifiable->name . ',')
                    ->line('To start using the webshop, please verify your email address below.')
                    ->action('Activate account', route('active', $this->activation->token))
                    ->line('Thank you for using our webshop!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
