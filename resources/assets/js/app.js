
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Turbolinks.start();

let ShoppingCart = {
    addListener: function () {
        $('body')
            .on('click', '.add-to-cart', function (e) {
               e.preventDefault();
               ShoppingCart.add($(this));
            })
            .on('input', '.product-amount', function (e) {
                ShoppingCart.update($(this));
            });
    },
    add: function ($item) {
        let url = $item.attr('href');

        axios.post(url)
            .then(function (response) {
                ShoppingCart.loadData(response.data);
            });
    },
    loadData: function (data) {
        let $modal = $('#product-modal');
        $modal.modal('show');

        let href = '/products/' + data.last.product.slug;

        $modal.find('.product-link').attr('href', href);
        $modal.find('.product-name').text(data.last.product.name);
        $modal.find('.product-price').text(data.last.product.price);
        $modal.find('.product-image').attr('src', data.last.product.image_path);

        if (data.total == 1) {
            $modal.find('.cart-items').text(data.total + " product");
        } else {
            $modal.find('.cart-items').text(data.total + " products");
        }
        $modal.find('.cart-costs').text("€" + data.total_price);

        $('.user-cart .cart-items').text(data.total);
    },
    update: function ($item) {
        let url = $item.closest('tr').data('url');

        axios.put(url, {amount: $item.val()});
    }
};

$(function() {
    $('.product-description').trumbowyg({
        autogrow: true,
        btns: [
            'btnGrp-semantic',
            ['link'],
            'btnGrp-justify',
            'btnGrp-lists',
            ['horizontalRule'],
            ['removeformat']
        ]
    });

    $(".selectable-group").chosen({
        create_option: true,
        persistent_create_option: true,
        create_option_text: 'add',
    });

    $('.dropdown-toggle').dropdown();

    ShoppingCart.addListener();
});
