@extends('layouts.app')

@section('breadcrumbs')
    {!! Laracrumbs::render($order) !!}
@endsection

@section('content')
    <div class="col-md-3">
    </div>
    <div class="col-md-9">
        <p>
            ID: #{{ $order->id }}
        </p>
        <p>
            Ordered by:
            @if ($order->user)
                {{ $order->user->name }}
            @else
                {{ $order->email }}
            @endif
        </p>
        <p>
            Order status: {{ $order->status }}
        </p>
        <p>
            Total price: &euro; {{ $order->total_price }}
        </p>
        <p>
            Order placed on: {{ $order->created_at }}
        </p>
        <table class="table table-striped">
            <tr>
                <th>
                    Product
                </th>
                <th>
                    Amount
                </th>
                <th>
                    Price
                </th>
                <th>
                    Total Price
                </th>
            </tr>
            @foreach($order->products as $product)
                <tr>
                    <td>
                        <a href="{{ route('products.show', $product) }}">
                            {{ $product->name }}
                        </a>
                    </td>
                    <td>
                        {{ $product->pivot->amount }}
                    </td>
                    <td>
                        {{ money($product->price) }}
                    </td>
                    <td>
                        {{ money($product->price * $product->pivot->amount) }}
                    </td>
                </tr>
            @endforeach
        </table>
        @if (currentUser()->isAdmin())
            <a href="{{ route('orders.edit', $order) }}" class="btn btn-primary">Edit Order</a>
        @endif
    </div>
@endsection