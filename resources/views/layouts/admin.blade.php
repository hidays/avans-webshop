@extends('layouts.app')

@section('content')
    @include('layouts.menus.admin')

    @yield('admin-content')
@endsection