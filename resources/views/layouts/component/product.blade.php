<div class="col-sm-4 col-lg-4 col-md-4">
    <div class="thumbnail">
        <img src="{{ $product->image_path }}" alt="" width="auto" height="150px">
        <div class="caption">
            <h4 class="pull-right">&euro; {{ $product->price }}</h4>
            <h4><a href="{{ route('products.show', $product->slug) }}">{{ $product->name }}</a>
            </h4>
            <p>{{ $product->description }}</p>
        </div>
        <div class="ratings pull-left">
            <span class="glyphicon glyphicon-star"></span>
            <span class="glyphicon glyphicon-star"></span>
            <span class="glyphicon glyphicon-star"></span>
            <span class="glyphicon glyphicon-star"></span>
            <span class="glyphicon glyphicon-star-empty"></span>
            <p>{{ mt_rand(2, 20) }} reviews</p>
        </div>
        <div class="cart-options pull-right">
            <a href="{{ route('products.add', $product->slug) }}" class="btn btn-primary btn-lg add-to-cart"
               data-turbolinks="false">
                <span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span>
            </a>
        </div>
        <div class="clearfix"></div>
    </div>
</div>