<nav class="navbar navbar-default">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{ url('/home') }}">{{ config('app.name') }}</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                {!! menu(\App\Library\Menu\MenuLoader::MAIN_MENU) !!}
            </ul>
            <form action="{{ route('search.index') }}" method="get" class="navbar-form navbar-left">
                <div class="input-group">
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Search" name="query"
                               value="{{ $searchQuery or '' }}">
                    </div>
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="submit">
                            <span class="glyphicon glyphicon-search"></span>
                        </button>
                    </span>
                </div>
            </form>
            <!-- Right Side Of Navbar -->
            <ul class="nav navbar-nav navbar-right">
                <!-- Authentication Links -->
                @if (Auth::guest())
                    {!! menu(\App\Library\Menu\MenuLoader::AUTH_MENU) !!}
                @else
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            <span class="glyphicon glyphicon-user"></span>
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu" role="menu">
                            {!! menu(\App\Library\Menu\MenuLoader::AUTH_MENU) !!}
                        </ul>
                    </li>
                @endif
                @include('layouts.menus.shopping')
            </ul>
        </div>
    </div>
</nav>