@extends('layouts.admin')

@section('breadcrumbs')
    {!! Laracrumbs::render() !!}
@endsection

@section('admin-content')
    <div class="col-md-9">
        <p>
            <a href="{{ route('products.create') }}" class="btn btn-default">Create product</a>
        </p>
        <table class="table table-striped">
            <tr>
                <th>
                    #
                </th>
                <th>
                    Name
                </th>
                <th>
                    Category
                </th>
            </tr>
            @foreach($products as $product)
                <tr>
                    <td>
                        {{ $product->id }}
                    </td>
                    <td>
                        <a href="{{ route('products.show', $product) }}">
                            {{ $product->name }}
                        </a>
                    </td>
                    <td>
                        <a href="{{ route('categories.show', $product->category) }}">
                            {{ $product->category->name }}
                        </a>
                    </td>
                </tr>
            @endforeach
        </table>
        {{ $products->links() }}
    </div>
@endsection