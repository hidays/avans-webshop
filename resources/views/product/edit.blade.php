@extends('layouts.admin')

@section('breadcrumbs')
    {!! Laracrumbs::render($product) !!}
@endsection

@section('admin-content')
    <div class="col-md-6">
        <form action="{{ route('products.update', $product) }}" method="post" enctype="multipart/form-data">
            {{ method_field('PUT') }}
            {{ csrf_field() }}

            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                <label for="name">Product Name</label>
                <input type="text" class="form-control" id="name" name="name"
                       value="{{ old('name', $product->name) }}">

                @if ($errors->has('name'))
                    <p class="help-block has-error">
                        <strong>{{ $errors->first('name') }}</strong>
                    </p>
                @endif
            </div>

            <div class="form-group{{ $errors->has('category_id') ? ' has-error' : '' }}">
                <label for="category_id">Category of</label>
                <select class="selectable-group form-control" id="category_id" name="category_id">
                    <option value="" disabled selected>Select a category</option>
                    @foreach($categories as $category)
                        <option value="{{ $category->id }}" {{ selected($category, $product->category_id) }}
                            class="category">{{ $category->name }}</option>

                        @foreach($category->subCategories as $subCategory)
                            <option value="{{ $subCategory->id }}" {{ selected($subCategory, $product->category_id) }}
                                class="item">{{ $subCategory->name }}</option>
                        @endforeach
                    @endforeach
                </select>

                @if ($errors->has('category_id'))
                    <p class="help-block has-error">
                        <strong>{{ $errors->first('category_id') }}</strong>
                    </p>
                @endif
            </div>

            <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                <label for="description">Description</label>
                <textarea class="form-control product-description" id="description"
                          name="description">{{ old('description', $product->description) }}</textarea>

                @if ($errors->has('description'))
                    <p class="help-block has-error">
                        <strong>{{ $errors->first('description') }}</strong>
                    </p>
                @endif
            </div>

            <div class="form-group{{ $errors->has('cover') ? ' has-error' : '' }}">
                <label for="cover">Product image cover</label>
                <input type="file" id="cover" name="cover">
                <p class="help-block">This image is used as cover.</p>

                @if ($errors->has('cover'))
                    <p class="help-block has-error">
                        <strong>{{ $errors->first('cover') }}</strong>
                    </p>
                @endif
            </div>

            <div class="form-group{{ $errors->has('price') ? ' has-error' : '' }}">
                <label for="price">Price (in euros)</label>
                <div class="input-group">
                    <div class="input-group-addon">&euro;</div>
                    <input type="number" class="form-control" id="price" name="price" min="0" step="any"
                           value="{{ old('price', $product->price) }}">
                </div>

                @if ($errors->has('price'))
                    <p class="help-block has-error">
                        <strong>{{ $errors->first('price') }}</strong>
                    </p>
                @endif
            </div>

            <div class="form-group{{ $errors->has('stock') ? ' has-error' : '' }}">
                <label for="stock">Stock</label>
                <input type="number" class="form-control" id="stock" name="stock" min="0"
                       value="{{ old('stock', $product->stock) }}">
                <p class="help-block">The amount of products currently in stock.</p>

                @if ($errors->has('stock'))
                    <p class="help-block has-error">
                        <strong>{{ $errors->first('stock') }}</strong>
                    </p>
                @endif
            </div>

            <button type="submit" class="btn btn-primary">Update</button>
        </form>
    </div>
@endsection