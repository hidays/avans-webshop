@extends('layouts.app')

@section('breadcrumbs')
    {!! Laracrumbs::render() !!}
@endsection

@section('content')

    <div class="col-md-3">
        @include('layouts.menus.categories')
    </div>

    <div class="col-md-9">

        <div class="row">

            @foreach($products as $product)
                @include('layouts.component.product', compact('product'))
            @endforeach

        </div>

    </div>
@endsection
