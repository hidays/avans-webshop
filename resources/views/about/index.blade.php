@extends('layouts.app')

@section('breadcrumbs')
    {!! Laracrumbs::render() !!}
@endsection


@section('content')
    <div class="col-md-3">
        @include('layouts.menus.categories')
    </div>

    <div class="col-md-9">

        <div class="row">

            {!! $about !!}

        </div>

    </div>
@endsection
