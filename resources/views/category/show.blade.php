@extends('layouts.admin')

@section('breadcrumbs')
    {!! Laracrumbs::render($category) !!}
@endsection

@section('admin-content')
    <div class="col-md-9">
        <p>
            {{ $category->name }}
        </p>
        <p>
            @if(is_null($category->subCategoryOf))
                Sub categories:
                @foreach($category->subCategories as $subCategory)
                    <a href="{{ route('categories.show', $subCategory) }}">
                        {{ $subCategory->name }}</a>{{ ! $loop->last ? ', '  : '' }}
                @endforeach
            @else
                Category of:
                <a href="{{ route('categories.show', $category->subCategoryOf) }}">
                    {{ $category->subCategoryOf->name }}
                </a>
            @endif
        </p>

        <a href="{{ route('categories.edit', $category) }}" class="btn btn-default">Edit</a>
        <a href="{{ route('categories.destroy', $category) }}" class="btn btn-warning">Delete</a>
    </div>
@endsection