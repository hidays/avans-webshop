@extends('layouts.admin')

@section('breadcrumbs')
    {!! Laracrumbs::render() !!}
@endsection

@section('admin-content')
    <div class="col-md-6">
        <form action="{{ route('categories.store') }}" method="post">
            {{ csrf_field() }}

            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                <label for="name">Category</label>
                <input type="text" class="form-control" id="name" name="name" placeholder="Category 1"
                       value="{{ old('name') }}">
                @if ($errors->has('name'))
                    <p class="help-block">
                        <strong>{{ $errors->first('name') }}</strong>
                    </p>
                @endif
            </div>

            <div class="form-group{{ $errors->has('category_id') ? ' has-error' : '' }}">
                <label for="category_id">Category of</label>
                <select class="form-control" id="category_id" name="category_id">
                    <option value="0" selected>None</option>
                    @foreach($categories as $category)
                        <option value="{{ $category->id }}">{{ $category->name }}</option>
                    @endforeach
                </select>
                @if ($errors->has('category_id'))
                    <p class="help-block">
                        <strong>{{ $errors->first('category_id') }}</strong>
                    </p>
                @endif
            </div>

            <button type="submit" class="btn btn-primary">Create</button>
        </form>
    </div>
@endsection