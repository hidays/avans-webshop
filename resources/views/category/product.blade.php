@extends('layouts.app')

@section('breadcrumbs')
    {!! Laracrumbs::render($category) !!}
@endsection

@section('content')
    <div class="col-md-3">
        <div class="list-group">
            @if ($subCategory)
                <a href="{{ route('categories.products', $category->subCategoryOf) }}"
                   class="list-group-item{{ $selectedCategory == $category->subCategoryOf->id ? ' active' : '' }}">
                    {{ $category->subCategoryOf->name }}
                </a>
                @foreach($category->subCategoryOf->subCategories as $item)
                    <a href="{{ route('categories.products', $item) }}"
                       class="list-group-item{{ $selectedCategory == $item->id ? ' active' : '' }}">
                        <span class="sub-category">{{ $item->name }}</span>
                    </a>
                @endforeach
            @else
                <a href="{{ route('categories.products', $category) }}"
                   class="list-group-item{{ $selectedCategory == $category->id ? ' active' : '' }}">
                    {{ $category->name }}
                </a>
                @foreach($category->subCategories as $item)
                    <a href="{{ route('categories.products', $item) }}"
                       class="list-group-item{{ $selectedCategory == $item->id ? ' active' : '' }}">
                        <span class="sub-category">{{ $item->name }}</span>
                    </a>
                @endforeach
            @endif
        </div>
    </div>
    <div class="col-md-9">
        <div class="row">
            @foreach($products as $product)
                @include('layouts.component.product', compact('product'))
            @endforeach
        </div>
        <div class="row">
            <div class="col-md-12">
                {{ $products->links() }}
            </div>
        </div>
    </div>
@endsection