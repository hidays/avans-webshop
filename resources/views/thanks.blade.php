@extends('layouts.app')

@section('breadcrumbs')
    {!! Laracrumbs::render() !!}
@endsection

@section('content')

    <div class="col-md-3">
        @include('layouts.menus.categories')
    </div>

    <div class="col-md-9">

        <div class="row">

            <p>
                Thanks you for choosing us. Your <a href="{{ route('orders.show', $order) }}">order</a> has been placed.
            </p>

        </div>

    </div>
@endsection
